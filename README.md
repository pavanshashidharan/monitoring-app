
# Website Monitoring Application  
  
 
The aim is to create a scalable web monitor application that feeds information about website availability to an Kafka instance. It will act as a producer that periodically checks target websites for their status and sends the results to a Kafka topic.  
  
  
The website checker should perform website status checks periodically and collect:  
  

 1. Total response time
 2. HTTP status code, if the request completes
    successfully 
 3. Whether the response body matches an optional regex
    check that can be passed as config to the program

This info should be written to the Kafka topic for later consumption.  
  
  
## Developer Preview  
  
I found some valuable resources on many youtube channel which would help understand the concept to complete this application.

Want to take this opportunity to apprecaite the regular workshops conducted on youtube.

[# Aiven Workshop 1: Learn Apache Kafka with Python](https://www.youtube.com/watch?v=MNq605WhvhY)

[# Aiven workshop 2: Learn Apache Kafka with Python](https://www.youtube.com/watch?v=0giSfPI0UnM)

This python application runs based on the website URL provided.  
  
Once the Target URL is added, this application pushes the following data to Kafka instace:  
1) Status Code - (If the sent request has a response back)   
2) Total Response Time
3) Whether the regex provided matches with the response body

In order to validate, the response body if matches an optional regex, I check for the 'title' tag in the response to satisfy one of the SEO criteria (Every SEO friendly site would have this title tag ).
- It returns *True*,  if the *title* tag exists in the response body. ***(It is a SEO friendly site)***
- Returns *False*, if 'title' tag is missing ***(Not a SEO friendly site)***

Be aware that **title** are added in all the SEO optimised websites. But the main requirement is to validate from response body with a REGEX, so I used this tag to check if the URL of the wesbite is SEO optimised or not. This is a boolean type data added to the Kafka topic.

This is a simple application with no complexity involved, trying to stick to the requirements. 
  
## Requirements  
  
1) Python3  
2) The latest version of PIP installed  
3) Preferred IDE editor - VS Code
4) Hostname, port number and other details for your Kafka instance 
5) Need to have the topic created beforehand in Apache Kafka or the `kafka.auto_create_topics_enable` parameter should be enabled
  
## Installation  
  
 1. Create a folder '***workspace***' and open this folder in your editor (VS Code preffered). **Open the editor terminal**.
 
 2. Go to your 'workpace' directory in the editor terminal and git pull the code using the command below:  
 ***- % cd into the 'workspace' directory in the editor teminal before proceeding. You can copy n paste all the commands in this file.***
% cd workspace
 ```
git clone git@gitlab.com:pavanshashidharan/monitoring-app.git
```  

2. Create virtualenv
```
python -m venv myenv
```  

 3. Enable virtualenv (*Please setup your own virtualenv before enabling*)
```
source myenv/bin/activate
```  
4. Go inside the app main directory 'monitoring-app'.

```
cd monitoring-app/
```  

5. Install the dependencies from requirements.txt  

```
pip install -r requirements.txt
```  
6. Optional, upgrade pip, if alered:
```
pip install --upgrade pip
```  
7. Duplicate existing 'env.example' file and name it '.env'  
```
cp env.example .env
```  
8.	 Once .env file is created, provide the secure information for the Kafka as per the example below:
I suggest using 'SSL' security protocol for this project. Hence predefined in my example.env file.

    HOSTNAME="<MY_KAFKA_HOST>" 
    PORT="<MY_KAFKA_PORT>"
    
    <Include other variables from 'env.example' file>
    

9. Update  '.env' file with all the required variables assigned.
10. Add your SSL ceritificates to the 'sslcerts' folder to authenticate the connection to Kafka:
	 - ***ca.pem***
	 - ***service.key***
	 - ***service.cert*** 
	 
	 *Please do not proceed without the SSL files. Python test  at the end of this section will fail.*
	 
11. You need to provide a website URL that you want to monitor. You can find this in 'producer.py' file in main directory.
> url_to_monitor = '<Add URL here with http/https>'

I added this above URL variable in the code so it is easy to change the URL to test other URL's as compared to adding this variable in the .env file. For easy access during testing. To make the application more secure, we can even consider moving this to .env.

 11. Make sure the URL provided includes 'http' or 'https'. Otherwise, it will give an error.
 12. Set the 'time_delay' parameter just below where you add the URL to monitor. This defines how often you would like to feed the website availability status to our Kafka topic

> delay_time = 2   # Added 2 secs delay inorder to make it easy for testing.

13. Run python test to check if nothing is broken. Copy and paste below code n the terminal to confirm.
```
pytest tests/
```  
Before proceeding, the all the test conditions above must pass without errors.


## Steps to run the appication  

1) Now we have to run below 2 python files simultaneous to observe the data feed. Before doing this please configure your editor to run 2 split terminals:
- ***consumer.py***

	 - Run this file first, in the one of the split editor terminal
	 - ```% python consumer.py ```  
	 - This is start listening to any new changes to our Kafka Topic. 

- ***producer.py***
			 

	 - Once the above '*consumer.py*' is running in one of the split terminal. We need to run '*producer.py*' next in other split terminal.
	 - ``% python producer.py ``
	 - This will start feeding the data to our Kafka topic.
	 - You can see this data appear in the split terminal where '*consumer.py*' is actively running.

You will start seeing the data in *consumer.py* terminal in the interval of 3 seconds as in the snippit below:

```
/Users/pavanshashidharan/Documents/xxxxxxxx/venv/bin/python /Users/pavanshashidharan/Documents/xxxxxxx/monitoring-app/consumer.py 
0:9: k={'website': 'https://test.io/'} v={'status_code': 200, 'response_time': 1.739733, 'regex_match_status': True}
0:10: k={'website': 'https://test.io/'} v={'status_code': 200, 'response_time': 0.359712, 'regex_match_status': True}
``` 

I hope the documentation was clear enough to get you seeing the application working on your system. Please let me know if there is any improvements or further updates is needed. I am more than happy to do it. You can see my contact information in the next step.

  
## Author / Developer
  
Pavan Shashidharan  
pavanshashidharan@gmail.com  
+64 221668145
