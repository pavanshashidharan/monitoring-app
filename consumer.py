import os
import sys
import time
from confluent_kafka import KafkaException, KafkaError

from connection import connect


def main():
    consumer = connect.consumer_connect()
    running = True

    try:
        consumer.subscribe([os.getenv("KAFKA_TOPIC_NAME")])
        print('Listening to our Kafka Topic...')
        while running:
            msg = consumer.poll(timeout=1.0)
            if msg is None: continue

            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # End of partition event
                    sys.stderr.write('%% %s [%d] reached end at offset %d\n' %
                                     (msg.topic(), msg.partition(), msg.offset()))
                elif msg.error():
                    raise KafkaException(msg.error())
            else:
                print("%d:%d: k=%s v=%s" % (
                    msg.partition(),
                    msg.offset(),
                    msg.key(),
                    msg.value()))
    except KeyboardInterrupt:
        print("Shutdown requested...exiting")
    finally:
        # Close down consumer to commit final offsets.
        consumer.close()


if __name__ == '__main__':
    main()
