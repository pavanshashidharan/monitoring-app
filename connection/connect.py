import os
import json
from dotenv import load_dotenv
from confluent_kafka import SerializingProducer, DeserializingConsumer

load_dotenv()  # Load the protected information from .env file
hostname = os.getenv("HOSTNAME")
port = os.getenv("PORT")
server_host = os.getenv("SERVER")
producer_client_id = os.getenv("PRODUCER_CLIENT_ID")
consumer_client_id = os.getenv("CONSUMER_CLIENT_ID")
consumer_group_id = os.getenv("CONSUMER_GROUP_ID")
security_protocol = os.getenv("SECURITY_PROTOCOL")
ca_location = os.getenv("SSL_CA_CERT")
cert_location = os.getenv("SSL_SERVICER_CERT")
key_location = os.getenv("SSL_SERVICER_KEY")


# JSON format serializer to feed to Topics
def producer_json_serializer(msg, s_obj):
    return json.dumps(msg).encode('ascii')

def consumer_json_serializer(msg, s_obj):
    return json.loads(msg.decode('ascii'))


# Configure the kafka instance connection
def producer_connect():
    """
    Using the connection details, configure the Kafka Producer connection

    Returns producer successfully created

    """
    producer_conf = {
        'bootstrap.servers': server_host,
        'client.id': producer_client_id,
        'security.protocol': security_protocol,
        'ssl.ca.location': ca_location,
        'ssl.certificate.location': cert_location,
        'ssl.key.location': key_location,
        'value.serializer': producer_json_serializer,
        'key.serializer': producer_json_serializer
    }
    return SerializingProducer(producer_conf)


def consumer_connect():
    """
    Using the connection details, configure the Kafka Consumer connection

    Returns Consumer successfully created
    """
    consumer_conf = {
        'bootstrap.servers': hostname + ":" + port,
        'client.id': consumer_client_id,
        'group.id': consumer_group_id,
        'security.protocol': security_protocol,
        'ssl.ca.location': ca_location,
        'ssl.certificate.location': cert_location,
        'ssl.key.location': key_location,
        'value.deserializer': consumer_json_serializer,
        'key.deserializer': consumer_json_serializer
    }

    return DeserializingConsumer(consumer_conf)
