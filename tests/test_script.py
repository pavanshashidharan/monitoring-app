import unittest
from validation import validate_url
from connection import connect
from monitoring import monitor

class ApplicationTesting(unittest.TestCase):
    # test function

    def test_connection(self):
        producer = connect.producer_connect()
        self.assertTrue(producer)

    def test_validate_url(self):
        url = 'https://test.com'
        validation = validate_url.validate(url)
        assert validation['status'] == 'Valid'

    def test_check_availability(self):
        url = 'https://google.com'
        producer = connect.producer_connect()
        data = monitor.check_availability(url, producer)
        message = "Data for the URL was generated successfully"
        self.assertTrue(data, message)


if __name__ == '__main__':
    unittest.main()