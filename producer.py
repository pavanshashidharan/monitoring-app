import time
from connection import connect
from monitoring import monitor
from validation import validate_url

# URL to monitor
url_to_monitor = '<Add URL here with http/https>'

# Time delay on how frequent to send data to kafka instance
delay_time = 2


def main():
    validation = validate_url.validate(url_to_monitor)  # Check if the URL is valid
    producer = connect.producer_connect()  # Establish the producer connection
    if validation['status'] == 'Valid':
        print('Producer started to feed the data to the Kafka Topic. Please see the actively running consumer.py to see the output.')
        while True:
            feed_status = monitor.check_availability(url_to_monitor, producer)  # Monitor the website URL to feed to Kafka
            time.sleep(delay_time)
            if not feed_status:
                break
    else:
        print('Invalid website URL. Please check the website url again.')
        exit()


if __name__ == '__main__':
    main()
