import os
import re
import requests


def check_availability(url, producer):
    """
    Get website availability status for the provided URL and feed data to Kafka

    Returns 'True' if the data is successfully sent to Kafka topic

    Parameters:
        url (string): This the url of the site we want to get data for
        producer: This is the producer connection

    Returns:
        Boolean

    """

    try:
        response = requests.get(url, timeout=3)
        response_data = response.text
        response_time = response.elapsed.total_seconds()
        title_regex_pattern = "<title>(.+?)</title>"  # Regex pattern
        regex_match_status = match_regex_with_response(title_regex_pattern,
                                                       response_data)  # Check for regex in response

        # Send data to the Kafka topic
        producer.produce(
            os.getenv("KAFKA_TOPIC_NAME"),
            key={"website": url},
            value={"status_code": response.status_code, "response_time": response_time,
                   "regex_match_status": regex_match_status}
        )
        producer.flush()
        # print(f"Response Code:{response.status_code}, response_time Time: {response_time}")
        return True
    except requests.exceptions.HTTPError as http_error:
        print("Http Error:", http_error)
        return False
    except requests.exceptions.ConnectionError as connection_error:
        print(f"Error Connecting to {url}. \nError information: {connection_error}")
        return False
    except requests.exceptions.Timeout as timeout_error:
        print("Timeout Error:", timeout_error)
        return False


def match_regex_with_response(pattern, response):
    """
    Check if the regex matches with the response body
    """
    result = re.search(pattern, response)
    if result.group(1):
        return True
    return False
