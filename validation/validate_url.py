import re

def validate(url):
    """
    Validate if the URL to monitor is in the right format

    Returns 'result' dictionary, with data if the is URL is valid or invalid

    Parameters:
        url (string): This is the website url we want to validate format

    Returns:
        Dictionary Object

    """

    result = {}
    regex = "^((http|https)://)[-a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)$"
    r = re.compile(regex)
    if re.search(r, url):
        result["status"] = 'Valid'
        result["url"] = url
        return result
    else:
        result["status"] = 'Invalid'
        result["url"] = url
        return result
